var peliculas = 
[
    { 
    "id" : 1,
    "nombre"  :  "El señor de los anillos", 
    "genero"   :  "Accion",
    "año" : "2010"
    },
    { 
    "id" : 2,
    "nombre"  :  "Regreso al Futuro", 
    "genero"   :  "Accion",
    "año" : "1982"
    },
    {
    "id" : 3,
    "nombre"  :  "Casablanca", 
    "genero"   :  "Suspense",
    "año" : "1949"
    }
]

var container = document.querySelector('#dynamic');
var editando = false;
var registro;

function refrescar()
{
    if ( container.hasChildNodes())
    {
        while ( container.childNodes.length > 2 )
        {
            container.removeChild( container.lastChild );
        }
    }

    for (var i = 0;i<peliculas.length;i++)
    {
        var linea = document.createElement("tr");

        var columna1 = document.createElement("td");
        columna1.innerText = (peliculas[i].id);
        var columna2 = document.createElement("td");
        columna2.innerText = (peliculas[i].nombre);
        var columna3 = document.createElement("td");
        columna3.innerText = (peliculas[i].genero);
        var columna4 = document.createElement("td");
        columna4.innerText = (peliculas[i].año);
        var columna5 = document.createElement("td");
        var botonEditar = document.createElement("button");
        botonEditar.innerText = ("Editar");
        botonEditar.addEventListener("click", Editar, false);
        columna5.appendChild(botonEditar);
        var botonBorrar = document.createElement("button");
        botonBorrar.innerText = ("Borrar");
        botonBorrar.addEventListener("click", Borrar, false);
        columna5.appendChild(botonBorrar);

        linea.appendChild(columna1);
        linea.appendChild(columna2);
        linea.appendChild(columna3);
        linea.appendChild(columna4);
        linea.appendChild(columna5);
        
        container.appendChild(linea); 
    }
};
refrescar();

var campoNombre = document.querySelector('#nombre');
var campoGenero = document.querySelector('#genero');
var campoAnio = document.querySelector('#anio');
var botonGuardar = document.querySelector('#Guardar');
botonGuardar.addEventListener("click", Guardar, false);

function Guardar(event)
{
    console.log(editando);
    if (editando == true)
    {
        for (var i = 0; i<peliculas.length;i++)
        {
            if(peliculas[i].id == registro)
            {
                peliculas[registro-1].nombre = campoNombre.value;
                peliculas[registro-1].genero = campoGenero.value;
                peliculas[registro-1].año = campoAnio.value;
            }
        }
        refrescar();
    }
    else
    {
        var lastId = peliculas[peliculas.length-1].id;
        peliculas.push(
            { id: lastId+1,
            nombre: campoNombre.value,
            genero: campoGenero.value,
            año: campoAnio.value
            });
            
        refrescar();
    }
    editando = false;
}

function Editar(event)
{
    refrescar();
    event.target.nextSibling.style.display = "none";
    editando = true;

    

    var padre = event.target.parentNode.parentNode;
    var hijos = padre.childNodes;

    registro = hijos[0].innerText;
    registro = parseInt(registro);
    campoNombre.value = hijos[1].innerText;
    campoGenero.value = hijos[2].innerText;
    campoAnio.value = hijos[3].innerText;

}

function Borrar(event)
{
    var padre = event.target.parentNode.parentNode;
    var hijos = padre.childNodes;
    
    padre.remove();
    registro = hijos[0].innerText;
    registro = parseInt(registro);

    for(var i = 0; i < peliculas.length;i++)
    {
        if(peliculas[i].id==registro)
        {
            peliculas.splice(i,1);   
        }
    }
}
